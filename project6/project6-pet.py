#Evan Kelly
#October 6, 2015
#Class project

import pet

myPet = pet.Pet('Steve', 'velociraptor', 5)
print('The name of my pet is', myPet.get_name())
print('The age of my pet is', str(myPet.get_age()))
print('My pet is a ', myPet.get_animal_type())
new_name = input('Enter a new name for you pet ')
myPet.set_name(new_name)
print('The new name of my pet is', myPet.get_name())
new_age = input('Enter a new age for your pet ')
myPet.set_age(new_age)
print('The new age of my pet is', myPet.get_age())
new_animal = input('Enter a new animal type for you pet')
myPet.set_animal_type(new_animal)
print('Your pet is now a(n) ', myPet.get_animal_type())