#Evan Kelly
#October 9, 2015

import Question
questions = ['What planet has the most moons?', 'What is the closest star that is not the sun?',
             'What is the speed of light?', 'What is the farthest man made object in space?',
             'Which president established the space program?', 'What does Voyager 1 and 2 carry?',
             'How much did the International Space Station cost?', 'What was the first human object in space?'
             , 'What was the first country to send a person to space?', 'What was the first exoplanet discovered?']
answer1 = ['Saturn', 'Sirius', '90,000,000 m/s', 'Voyager 1', 'Dwight Eisenhower', 'Beatles CDs', '$600 million',
           'Apollo 11', 'U.S.A', '51 Pegasi b']
answer2 = ['Jupiter', 'Ross 154', '568,395,038 m/s', 'Voyager 2', 'John F. Kennedy', 'Human DNA', '$3 billion',
           'German V2 rocket', 'Canada', 'WASP-17b']
answer3 = ['Neptune', 'Alpha Centauri', '347,003 km/s', 'The Galileo telescope', 'Richard Nixon',
           'Messages to extraterrestrials', '$237 billion', 'Hubble space telescope', 'Soviet Union', 'AB Pictoris']
answer4 = ['Mars', 'Epsilon Indi', '299,792,000 m/s', 'Orion Spacecraft', 'Ronald Reagan', 'Plants', '$150 billion',
           'Apollo 16', 'Russia', 'PSR B1257+12 B']
solution = [2, 3, 4, 1, 1, 3, 4, 2, 3, 1]
questionList = list()
player1 = 0
player2 = 0
x = 0
turns = 10

while x < turns:
    q = Question.Question(questions[x], answer1[x], answer2[x], answer3[x], answer4[x], solution[x])
    questionList.append(q)
    x += 1
turn = 0
while turn < turns:
    if turn%2 != 0:
        questionList[turn].ask()
        response = int(input('Player 2, enter a number '))
        questionList[turn].getCorrect()
        if questionList[turn].correct(response):
        #if response == solution:
            player2 += 1
            print('Correct')
        else:
            print('Incorrect')
    if turn%2 == 0:
        questionList[turn].ask()
        response = int(input('Player 1, enter a number '))
        questionList[turn].getCorrect()
        if questionList[turn].correct(response):
        #if response == solution:
            player1 += 1
            print('Correct')
        else:
            print('Incorrect')
    turn += 1

if x == turns:
    print('PLayer 1 score is', player1)
    print('Player 2 score is', player2)
    if player1 > player2:
        print('Player 1 wins')
    else:
        print('Player 2 wins')

